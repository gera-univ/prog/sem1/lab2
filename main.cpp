#include <iostream>

using namespace std;

void insertion_sort(double a[], int first, int last, int step)   {
    for (int i = first + 1; i < last; i+=step) {
        double k = a[i];
        int j = i - step;
        while (j >= first && a[j] > k)  {
            a[j+step] = a[j];
            j-=step;
        }
        a[j + step] = k;
    }
}

double safe_double_input() {
    string s;
    cin >> s;
    bool is_negative = false;
    bool has_point = false;
    for (auto c : s) {
        bool expect_digit = true;
        if (c == '-') {
            expect_digit = false;
            if (is_negative) {
                    cout << "Too many minuses!" << endl;
                    return safe_double_input();
            }
            else is_negative = true;
            continue;
        }
        if (c == '.') {
            expect_digit = true;
            if (has_point) {
                cout << "Too many points!" << endl;
                return safe_double_input();
            }
            else has_point = true;
            continue;
        }
        if (expect_digit && !isdigit(c)) {
            cout << "You entered characters that are not digits!" << endl;
            return safe_double_input();
        }
    }
    return stod(s);
}

int safe_natural_input() {
    string s;
    cin >> s;
    for (auto c : s) {
        if (!isdigit(c)) {
            cout << "Your input is incorrect. Please enter a natural value" << endl;
            return safe_natural_input();
        }
    }
    return stoi(s);
}

int main()  {
//    const int MAX_SIZE = 100000;
//    double * data = new double[MAX_SIZE];
//    int N = 1;
//    cout << "Task 11\nEnter the elements of the array. Press ctrl+d to terminate your input\n";
//    while (cin >> data[N]) {
//        cout << "data[" << N << "]:  " << data[N] << endl;
//        ++N;
//    }

    int N;
    cout << "How many numbers would you like to type? ";
    N = safe_natural_input(); ++N;
    double * data = new double[N];
    for (auto i = 1; i < N; ++i) {
        data[i] = safe_double_input();
        cout << "data[" << i << "]:  " << data[i] << endl;
    }

    cout << "- - -" << endl;

    double sum_of_negative = 0;
    int imin, imax;
    double minimal = data[1];
    double maximal = data[1];

    for (int i = 1; i < N; ++i) {
        if (data[i] < 0)
            sum_of_negative += data[i];
        if (data[i] >= maximal) {
            maximal = data[i];
            imax = i;
        }
        if (data[i] <= minimal) {
            minimal = data[i];
            imin = i;
        }
    }
    if (sum_of_negative < 0)
        cout << "The sum of negative elements is " << sum_of_negative << endl;
    else cout << "There are no negative elements in the array" << endl;
    int istart=min(imin, imax);
    int iend=max(imin, imax);
    if (iend - istart > 1)  {
        double product = 1;
        for (int i = istart+1; i < iend; ++i) {
            product*=data[i];
        }
        cout << "The product of the elements between data[" << imin << "]="
             << minimal << " and data[" << imax << "]="
             << maximal << " is " << product << endl;
    } else
        cout << "There are no elements between data[" << imin << "]="
             << minimal << " and data[" << imax << "]=" << maximal << endl;
    cout << "The array, but the even elements are sorted:\n";
    insertion_sort(data, 1, N, 2);
    for (int i = 1; i < N; ++i)
        cout << data[i] << " ";
    delete [] data;
    return 0;
}
